import sys
import os
import pathlib
from PyQt5.QtWidgets import QCheckBox
from PyQt5.QtWidgets import QDialog
from PyQt5.QtWidgets import QGroupBox
from PyQt5.QtWidgets import QLabel
from PyQt5.QtWidgets import QPushButton
from PyQt5.QtWidgets import QVBoxLayout

from  Indexer import  TERSchema
from PyQt5.QtWidgets import (QMainWindow, QTextEdit,
                             QAction, QFileDialog, QApplication,QMessageBox,qApp,QLineEdit,QGridLayout)
from PyQt5.QtGui import QIcon
from  PyQt5.QtCore import Qt, pyqtSlot


class Parametr(QMainWindow):
    doc =[]
    def __init__(self,file):
        super(Parametr,self).__init__()
        self.f = file
        self.initUI()
    def initUI(self):


        self.resize(500,400)
        self.textbox = QLineEdit(self)
        self.textbox.move(250, 40)
        self.search = QPushButton("Search", self)
        self.textbox.setAlignment(Qt.AlignCenter)
        self.show()

        self.doc.append(self)
    def search_click(self):
        textboxValue = self.textbox.text()
        QMessageBox.question(self, 'Message - pythonspot.com', "You typed: " + textboxValue, QMessageBox.Ok,
                             QMessageBox.Ok)
        self.textbox.setText("")

class Example(QMainWindow):
    file = 0
    def __init__(self):
        super(Example,self).__init__()
        self.initUI()

    def initUI(self):
        self.schema = 0
        self.resize(600, 400)
        self.textEdit = QTextEdit()
        self.setCentralWidget(self.textEdit)
        self.statusBar()
        self.setWindowTitle('Searcher')
        self.setWindowIcon(QIcon('web.ico'))
        openDialog = QAction( QIcon("open.ico"),"Open", self)
        openDialog.setShortcut('Ctrl+Q')
        openDialog.triggered.connect(self.showDialog)
        self.toolbar = self.addToolBar("Open")
        self.toolbar.addAction(openDialog)
        runAction = QAction(QIcon('run.ico'),"Run",self)
        runAction.setShortcut('Ctrl+Q')
        runAction.triggered.connect(self.run)
        self.toolbar = self.addToolBar('Run')
        self.toolbar.addAction(runAction)
        self.setWindowTitle('File dialog')
        self.show()
    def closeEvent(self, event):
        reply = QMessageBox.question(self, 'Message',
                                     "Are you sure to quit?", QMessageBox.Yes |
                                     QMessageBox.No, QMessageBox.No)

        if reply == QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()
            global  doc

    def showDialog(self):
        fname = QFileDialog.getOpenFileName(self, 'Open file', '/home')

        if fname[0]:
            self.file = open(fname[0], 'r')
            with self.file:
                data = self.file.read()
                self.textEdit.setText(data)

    def run(self):
        if(self.file == 0):
            reply = QMessageBox.question(self, 'Message',
                                         "Please choose the file", QMessageBox.Ok)
        else:
            dirPath = pathlib.Path(self.file.name)
            folder = dirPath.parent
            schema = TERSchema(folder,self.file.name)
            schema.index_document()
            a = App(schema)


class App(QDialog):
    global doc
    def __init__(self,indexer):
        super(App,self).__init__()
        self.indexer = indexer
        self.title = 'Search'
        self.left = 10
        self.top = 10
        self.width = 400
        self.height = 400
        self.searchLabel = QLabel("Search")
        self.searchField = QLineEdit()
        self.titleLabel = QLabel("Title")
        self.titleField = QLineEdit()
        self.descriptionLabel = QLabel("Description")
        self.descriptionField = QLineEdit()
        self.dateLabel = QLabel("Date")
        self.dateLabelFrom = QLabel("Date From")
        self.dateFieldFrom = QLineEdit()
        self.dateLabelTo = QLabel("Date To")
        self.dateFieldTo = QLineEdit()
        self.dateField = QLineEdit()
        self.titleField.setDisabled(True)
        self.descriptionField.setDisabled(True)
        self.dateFieldFrom.setDisabled(True)
        self.dateFieldTo.setDisabled(True)
        self.cb = True
        self.cbInterval = True
        self.initUI()
        self.output = []
        global  doc
        doc = self
    def initUI(self):
        self.bt = QPushButton("Advanced Search")
        self.bt.setIcon(QIcon("disable.ico"))
        self.searchButton = QPushButton("Search")
        self.btDateInt = QPushButton("Date interval")
        self.btDateInt.setIcon(QIcon("disable.ico"))
        self.btDateInt.clicked.connect(self.dateIntrevalBox)
        self.searchButton.clicked.connect(self.search)
        self.bt.clicked.connect(self.advancedSearchBox)
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.createGridLayout()
        windowLayout = QVBoxLayout()
        windowLayout.addWidget(self.horizontalGroupBox)
        self.setLayout(windowLayout)
        self.show()


    def advancedSearchBox(self):
        if self.cb ==True:
            self.cb = False
            self.activateFields(False)
            self.bt.setIcon(QIcon("enable.ico"))
            self.searchField.clear()
            self.searchField.setDisabled(True)

        else:
            self.cb = True
            self.titleField.clear()
            self.descriptionField.clear()
            self.activateFields(True)
            self.searchField.setDisabled(False)
            self.bt.setIcon(QIcon("disable.ico"))
    def dateIntrevalBox(self):
        if self.cbInterval ==True:
            self.cbInterval = False
            self.activateDateField(False)
            self.dateField.clear()
            self.btDateInt.setIcon(QIcon("enable.ico"))
        else:
            self.cbInterval = True
            self.activateDateField(True)
            self.dateFieldFrom.clear()
            self.dateFieldTo.clear()
            self.btDateInt.setIcon(QIcon("disable.ico"))
    def search(self):
        ex.textEdit.setText("")
        if(self.cb):
            self.getSearchText()
        else:
            self.getadvancedSearch()
        for i in self.output:
            ex.textEdit.append(str(i))
        self.output.clear()
    def getSearchText(self):
        str= self.searchField.text()
        if(self.cbInterval is True):
            if(self.dateField.text()== ""):
                self.indexer.parse_document(str,False,False)
                self.output = self.indexer.getResult()
            else:
                date_str = self.dateField.text()
                self.indexer.parse_document(str, True,False,date_str)
                self.output = self.indexer.getResult()
        elif(self.cbInterval is False):
            if (self.dateFieldFrom.text() == "" or self.dateFieldTo.text() == ""):
                self.indexer.parse_document(str, False,False)
                self.output = self.indexer.getResult()
            else:
                date_From = self.dateFieldFrom.text()
                date_To = self.dateFieldTo.text()
                self.indexer.parse_document(str, True,True,"",date_From,date_To)
                self.output = self.indexer.getResult()

    def getadvancedSearch(self):
        title_str = self.titleField.text()
        description_str = self.descriptionField.text()
        if(title_str is not "" and description_str is not ""):
            if(self.cbInterval is True):
                if (self.dateField.text() == ""):
                    self.indexer.advanced_parse(title_str,description_str,False,False)
                    self.output = self.indexer.getResult()
                else:
                    date_str = self.dateField.text()
                    self.indexer.advanced_parse(title_str,description_str,True,False,date_str)
                    self.output = self.indexer.getResult()
            if(self.cbInterval is False):
                if (self.dateFieldFrom.text() == "" or self.dateFieldTo.text() == ""):
                    self.indexer.advanced_parse(title_str,description_str, False,False)
                    self.output = self.indexer.getResult()
                else:
                    date_From = self.dateFieldFrom.text()
                    date_To = self.dateFieldTo.text()
                    self.indexer.advanced_parse(title_str,description_str, True, True, "", date_From, date_To)
                    self.output = self.indexer.getResult()
        elif(title_str is "" and description_str is not ""):
            if (self.cbInterval is True):
                if (self.dateField.text() == ""):
                    self.indexer.parse_single("description", description_str, False, False)
                    self.output = self.indexer.getResult()
                else:
                    date_str = self.dateField.text()
                    self.indexer.parse_single("description", description_str, True, False, date_str)
                    self.output = self.indexer.getResult()
            if (self.cbInterval is False):
                if (self.dateFieldFrom.text() == "" or self.dateFieldTo.text() == ""):
                    self.indexer.parse_single("description", description_str, False, False)
                    self.output = self.indexer.getResult()
                else:
                    date_From = self.dateFieldFrom.text()
                    date_To = self.dateFieldTo.text()
                    self.indexer.parse_single("description", description_str, True, True, "", date_From, date_To)
                    self.output = self.indexer.getResult()
        elif (title_str is not "" and description_str is ""):
            if (self.cbInterval is True):
                if (self.dateField.text() == ""):
                    self.indexer.parse_single("title", title_str, False, False)
                    self.output = self.indexer.getResult()
                else:
                    date_str = self.dateField.text()
                    self.indexer.parse_single("title", title_str, True, False, date_str)
                    self.output = self.indexer.getResult()
            if (self.cbInterval is False):
                if (self.dateFieldFrom.text() == "" or self.dateFieldTo.text() == ""):
                    self.indexer.parse_single("title", title_str, False, False)
                    self.output = self.indexer.getResult()
                else:
                    date_From = self.dateFieldFrom.text()
                    date_To = self.dateFieldTo.text()
                    self.indexer.parse_single("title", title_str, True, True, "", date_From, date_To)
                    self.output = self.indexer.getResult()
    def activateFields(self,bool):
        self.titleField.setDisabled(bool)
        self.descriptionField.setDisabled(bool)
    def activateDateField(self,bool):
        self.dateFieldTo.setDisabled(bool)
        self.dateFieldFrom.setDisabled(bool)
        self.dateField.setDisabled(not bool)
    def clearText(self):
        self.titleField.setText("")
        self.descriptionField.setText("")

    def createGridLayout(self):
        self.horizontalGroupBox = QGroupBox("Search by keywords")
        layout = QGridLayout()
        layout.setColumnStretch(1, 4)
        layout.setColumnStretch(2, 4)
        layout.addWidget(self.searchLabel, 0, 0)
        layout.addWidget(self.searchField, 0, 1)
        layout.addWidget(self.searchButton, 0, 2)
        layout.addWidget(self.titleLabel,1,0)
        layout.addWidget(self.titleField,1,1)
        layout.addWidget(self.bt, 0, 3)
        layout.addWidget(self.descriptionLabel, 2, 0)
        layout.addWidget(self.descriptionField, 2, 1)
        layout.addWidget(self.dateLabel, 3, 0)
        layout.addWidget(self.dateField, 3, 1)
        layout.addWidget(self.btDateInt, 3, 2)
        layout.addWidget(self.dateLabelFrom, 4, 0)
        layout.addWidget(self.dateFieldFrom, 4, 1)
        layout.addWidget(self.dateLabelTo, 5, 0)
        layout.addWidget(self.dateFieldTo, 5, 1)
        self.horizontalGroupBox.setLayout(layout)
    def closeEvent(self, event):
        global doc
        doc = None

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
