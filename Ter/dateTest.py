from datetime import datetime, timedelta
from whoosh import fields, index

schema = fields.Schema(title=fields.TEXT, content=fields.TEXT,
                       date=fields.DATETIME)
ix = index.create_in("indexdir", schema)

w = ix.writer()
w.add_document(title="Document 1", content="Rendering images from the command line",
               date=datetime.utcnow())
w.add_document(title="Document 2", content="Creating shaders using a node network",
               date=datetime.utcnow() + timedelta(days=1))

w.commit()