

from cx_Freeze import setup, Executable
import sys
import whoosh
import csv
import Date
import  Output
import  Indexer
import os
base = None
executables = [Executable("Main.py", base=base, icon="web.ico")]
setup(
    name = "Ter build",
    version = "0.1",
    description = "Ter build",
    options={"build_exe": {"packages": ["whoosh", "csv","os","pathlib","Date","Output","Indexer"], "include_files": ["web.ico","run.ico","open.ico","enable.ico","disable.ico","Date.py","Output.py","Indexer.py"]}},
    executables = executables)