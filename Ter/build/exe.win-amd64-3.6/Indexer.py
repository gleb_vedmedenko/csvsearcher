from whoosh.index import create_in
from whoosh.fields import *
from whoosh.qparser import QueryParser, MultifieldParser
from  Date import Date
from  datetime import datetime
from  Output import  Result
import csv
PATH_FOLDER = "C:\\Users\GlebV\Documents\TD"
PATH_CSV = "C:\\Users\GlebV\Documents\TD\Rss.csv"

class TERSchema:
    def __init__(self,path_folder,path_csv):
        self.pathfolder = path_folder
        self.pathcsv= path_csv
        self.fields = ["title","description","date","rss","author","link"]
        self.schema =Schema(title=TEXT(stored=True), description=TEXT(stored=True), date=DATETIME(stored=True), rss=ID(stored=True),
                        author=TEXT(stored=True),
                        link=ID(stored=True,unique=True))

        self.ix = create_in(self.pathfolder, self.schema)
        self.result = []
    def index_document(self):

        writer = self.ix.writer()
        with open(self.pathcsv) as csvfile:
            text_reader = csv.reader(csvfile)
            next(text_reader)
            doc = []
            for row in text_reader:
                if (doc.__len__() > 0):
                    self.indexLine(doc,writer)

                for i in row:
                    doc.append(i)


        writer.commit()
    def indexLine(self,docLine, writer):
        #d = Date("Sun, 11 Jun 2017 09:00:00 -0400")
        d = Date(docLine[2])
        #print(docLine[2])
        writer.add_document(title=docLine[0], description=docLine[1], date=d.num_date, rss=docLine[3],
                                author=docLine[4], link=docLine[5])
        docLine.clear()
    def parse_document(self,word,dataFlag,dateInterval,date="",dateFrom="",dateTo=""):
        from whoosh.qparser import MultifieldParser
        with self.ix.searcher() as searcher:

            if(dataFlag is False):
                query = MultifieldParser([self.fields[0],self.fields[1]], self.ix.schema).parse(word)
                #results = searcher.search(query)
                results = searcher.search(query)
                if(len(results) >  0):
                    for i in results:
                        res = Result()
                        self.parse_element(i,res)
            elif(dataFlag is True):
                if(dateInterval is False):
                     query = MultifieldParser([self.fields[0], self.fields[1]], self.ix.schema).parse("(title:{0} OR description:{0})  date:{1}".format(word,date))
                     # results = searcher.search(query)
                     results = searcher.search(query)
                     if (len(results) > 0):
                         for i in results:
                             res = Result()
                             self.parse_element(i, res)
                elif(dateInterval is True):
                    query = MultifieldParser([self.fields[0], self.fields[1]], self.ix.schema).parse(
                        "(title:{0} OR description:{0})  date:[{1} to {2}]".format(word, dateFrom,dateTo))
                    # results = searcher.search(query)
                    results = searcher.search(query)
                    if (len(results) > 0):
                        for i in results:
                            res = Result()
                            self.parse_element(i, res)
    def parse_single(self,element,textElement,dateFlag,dateInterval,date="",dateFrom="",dateTo=""):
        with self.ix.searcher() as searcher:

            if(dateFlag is False):
                titleQuerry = QueryParser(element, self.ix.schema).parse(textElement)
                # results = searcher.search(query)
                results = searcher.search(titleQuerry)
                if (len(results) > 0):
                    for i in results:
                        res = Result()
                        self.parse_element(i, res)

            elif(dateFlag is True):
                if (dateInterval is False):
                    query = MultifieldParser(element, self.ix.schema).parse("{0}:{1} date:{2}".format(element,textElement,date))
                    # results = searcher.search(query)
                    results = searcher.search(query)
                    if (len(results) > 0):
                        for i in results:
                            res = Result()
                            self.parse_element(i, res)
                elif (dateInterval is True):
                    query = MultifieldParser(element, self.ix.schema).parse(
                        "{0}:{1} date:[{2} to {3}]".format(element, textElement, dateFrom, dateTo))
                    # results = searcher.search(query)
                    results = searcher.search(query)
                    if (len(results) > 0):
                        for i in results:
                            res = Result()
                            self.parse_element(i, res)

    def advanced_parse(self,title,description,dataFlag,dateInterval,date="",dateFrom="",dateTo=""):
        with self.ix.searcher() as searcher:
            if (dataFlag is False):
                query = MultifieldParser([self.fields[0], self.fields[1]], self.ix.schema).parse("(title:{0} AND description:{1})".format(title,description))
                # results = searcher.search(query)
                results = searcher.search(query)
                if (len(results) > 0):
                    for i in results:
                        res = Result()
                        self.parse_element(i, res)
            elif (dataFlag is True):
                if (dateInterval is False):
                    query = MultifieldParser([self.fields[0], self.fields[1]], self.ix.schema).parse("(title:{0} AND description:{1})  date:{2}".format(title,description,date))
                    # results = searcher.search(query)
                    results = searcher.search(query)
                    if (len(results) > 0):
                        for i in results:
                            res = Result()
                            self.parse_element(i, res)
                elif (dateInterval is True):
                    query = MultifieldParser([self.fields[0], self.fields[1]], self.ix.schema).parse("(title:{0} AND description:{1})  date:[{2} to {3}]".format(title,description, dateFrom, dateTo))
                    # results = searcher.search(query)
                    results = searcher.search(query)
                    if (len(results) > 0):
                        for i in results:
                            res = Result()
                            self.parse_element(i, res)

    def advanced_test(self,title,descr):
        with self.ix.searcher() as searcher:
            query = MultifieldParser(["title","description"], self.ix.schema).parse("title:{0} description:{1}".format(title,descr))
            results = searcher.search(query)
            if (len(results) > 0):
                for i in results:
                    res = Result()
                    self.parse_element(i, res)
    def parse_date(self,field,date):
        from whoosh.qparser import QueryParser
        with self.ix.searcher() as searcher:
            query= QueryParser(field, self.ix.schema).parse(date)
            results = searcher.search(query)
            for i in results:
                print(i)
    def global_search(self,word,desc):
        self.advanced_parse(word,desc)
        #self.parse_document(word,date)
        #self.parse_document("title",word)
        #self.parse_document("description",word)
        # self.parse_document("author",word)

    def parse_element(self,element,result):
        resultField =""
        resultText =""
        for i in self.fields:
                resultField += i + ":"
                if i ==  "date":
                    try:
                        result.setDate(element[i])
                    except ValueError:
                         print("Wrong format of date")

                elif i =="title":
                    result.setTitle(element[i])
                else:
                    resultText +=  "\n" + element[i]
        result.setResult(resultText)
        self.result.append(result)
    def getResult(self):
        out_text = []
        for i in self.result:
            out_text.append("------------------------------------------")
            out_text.append("FULL INFO: " + str(i.result_str))
            out_text.append("TITLE: \n" + str(i.title))
            out_text.append("Date: \n" + str(i.date))
        self.result.clear()
        return out_text
#s = TERSchema(PATH_FOLDER,PATH_CSV)
#s.index_document()
#s.parse_date("date","[20170610 to 20180610]")
#s.global_search("Jeremy","turned")
#s.parse_title("Jeremy",True,"20170609")
#s.parse_document("House",True,True,"","20170506","20170610")
#s.advanced_parse("house","House",True,True,"","2016","2017")
#s.parse_single("title","Chalk Artist",False,False)
#s.getResult()
