from datetime import datetime


class Date:
    def __init__(self, string_date):

            self.str_date = string_date
            self.num_date = self.to_numeric(self.str_date)

    def to_numeric(self, string_date):

        d = datetime.strptime(string_date, ' %a, %d %b %Y %H:%M:%S %z')
        return d